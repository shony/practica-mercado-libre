import BaseModule from './basemodule'
import List from './list'
import Favorites from './favorites'

export default [
    {
        path: '/',
        component: BaseModule,
        name: 'Base Route',
        redirect: '/list',
        children: [
            {
                name: 'Mercado Libre List',
                path: 'list',
                component: List
            },
            {
                name: 'Favorites List',
                path: 'favorites',
                component: Favorites
            },
        ]
    }
]
