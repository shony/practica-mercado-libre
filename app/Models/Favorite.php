<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Favorite extends Model
{
    protected $collection = 'favorites_collection';
    protected $connection = 'mongodb';

    protected $fillable = [
        'id_ml',
        'title',
        'site_id',
        'price',
        'permalink',
        'thumbnail'
    ];
}