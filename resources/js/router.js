import Vue from 'vue'
import VueRouter from 'vue-router'
import config from './config'

// Modules Routes
let routes = []
import components from './components/routes'

// Concat Routes
routes = routes.concat(
    components
)
Vue.use(VueRouter)

const router = new VueRouter({
    //mode: 'history', //removes # (hashtag) from url
    base: config.publicUrl,
    fallback: true, //router should fallback to hash (#) mode when the browser does not support history.pushState
    routes,
    scrollBehavior() { return { x:0, y:0 } }
})

export default router
