<?php

namespace App\Helpers;

use GuzzleHttp\Client;
use App\Models\Token;

class MercadoLibreApi
{
    public static function getSearch($search, $offset)
    {
        $httpClient = new Client();
        $api_url = 'https://api.mercadolibre.com/sites/MLM/search';

        $headers = [
            'Authorization' => 'Bearer ' . Token::first()->access_token,        
            'Accept'        => 'application/json'
        ];

        $response = $httpClient->get($api_url,  [
            'query' => [
                'q' => $search,
                'offset' => $offset
            ],
            'headers' => $headers
        ]);

        $contents = json_decode($response->getBody(), true);

        return $contents;
    }

    public static function refreshToken()
    {
        $httpClient = new Client();
        $api_url = 'https://api.mercadolibre.com/oauth/token';

        $headers = [
            'Content-Type'  => 'application/x-www-form-urlencoded',        
            'Accept'        => 'application/json'
        ];

        $response = $httpClient->post($api_url,  [
            'form_params' => [
                'grant_type'    => 'refresh_token',
                'client_id'     => env('MERCADO_LIBRE_APP_ID'),
                'client_secret' => env('MERCADO_LIBRE_CLIENT_SECRET'),
                'refresh_token' => env('MERCADO_LIBRE_REFRESH_TOKEN')
            ],
            'headers' => $headers
        ]);

        $contents = json_decode($response->getBody(), true);

        return $contents;
    }
}
