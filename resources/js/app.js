import Vue from 'vue'
import router from './router'
import config from './config'

// Vue Fontawesome 
require('./fontawesome.js')

// Buefy
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
Vue.use(Buefy, {
    defaultIconPack: 'fas',
	defaultIconComponent: 'font-awesome-icon'
})

// ImageKit
import ImageKit from "imagekitio-vue"
Vue.use(ImageKit, {
	urlEndpoint: "https://ik.imagekit.io/ctyttbqpls/",
	// publicKey: "your_public_api_key",
	// authenticationEndpoint: "https://www.your-server.com/auth"
})

// Axios
import axios from 'axios'
axios.defaults.baseURL = config.publicUrl
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
window.axios = axios

// Set Global Vars
Vue.prototype.$publicUrl = config.publicUrl
Vue.prototype.$appName = config.appName
Vue.prototype.$token = config.token

// Vue Config
Vue.config.productionTip = false

// Components
import baseapp from './components/baseapp'

new Vue({
    el: '#app',
	router,
    components: { baseapp }
})
