<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FavoritesController;
use App\Http\Controllers\SiteController;
use App\Http\Controllers\MercadoLibreController;
use App\Http\Controllers\TestController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',               [SiteController::class, 'redirectToApp'])->name('home');
Route::get('/app',            [SiteController::class, 'home'])->name('home.app');
Route::get('/app/{route:.*}', [SiteController::class, 'home'])->name('home.app.2');

Route::middleware(['isAjax'])->group(function () {
    Route::post('/mercadolibre/search',  [MercadoLibreController::class, 'getSearch'])->name('mercadolibre.search');
    Route::get('/favorites/get',         [FavoritesController::class, 'getFavorites'])->name('favorites.get');
    Route::post('/favorites/create',     [FavoritesController::class, 'createFavorite'])->name('favorites.create');
    Route::post('/favorites/delete',     [FavoritesController::class, 'deleteFavorite'])->name('favorites.delete');
    Route::post('/mercadolibre/refresh', [MercadoLibreController::class, 'refreshToken'])->name('mercadolibre.refresh');
});

// Testing
Route::get('/test', [TestController::class, 'test'])->name('test');