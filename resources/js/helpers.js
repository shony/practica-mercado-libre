export function formatCurrency (value) {
    var val = parseFloat(value)
    return '$' + val.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")
}

export function numberWithCommas (x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}

export function capitalizeFirstLetter (string) {
    return string.charAt(0).toUpperCase() + string.slice(1)
}

export function textTruncate (str, length = 100, ending = '...') {
    if (str.length > length) {  
        return str.substring(0, length - ending.length) + ending; 
    } else { return str }  
}

export function setZoneScrollToBottom (id_element) {
    var container = document.querySelector(id_element)
    container.scrollTop = container.scrollHeight
}

export function setScrollToElement (id_element) {
    var element = document.querySelector(id_element)
    const y = element.getBoundingClientRect().top + window.scrollY
    window.scroll({ top: y, behavior: 'smooth' })
}

export function checkIfUserOnline (active) {
    if (active == 1) { return 'online' }
    else { return 'disconnected' }
}

export function getReadableDate (timestamp) {
    if (timestamp == null) { return '' }
    moment.locale(localStorage.getItem('language'))
    var now = moment()
    var last_online_at = moment(timestamp)
    var hours = now.diff(last_online_at, 'hours')
    if (hours == 0) {
        // Do with Days
    }
    return 'Last online ' + hours + ' hour(s) ago'
}

export function isValidFileSize (file, maxsize) {
    if (file.size > maxsize) { return false }
    { return true }
}

export function isValidImageExtension (file) {
    if (!/\.(jpg|jpeg|png|gif)$/i.test(file.name)) {
        return false
    } else { return true }
}