<!doctype html>
<html lang="{{ config('app.locale') }}">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE-edge">
	<meta name="csrf-token" id="csrf-token" content="{{ csrf_token() }}">
	<meta name="public_url" content="{{ url('/') }}/" />
	<meta name="app_name" content="{{ env('APP_NAME') }}" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	{{-- Styles --}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.2/css/bulma.min.css">
	<link rel="stylesheet" href="{{ asset('css/all.min.css') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">

</head>
<body>
    <!-- Content -->
    @yield('content')
    <!-- Content -->
</body>
</html>
