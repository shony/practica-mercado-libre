<div class="columns">
    <div class="column">
        <form method="POST" action="{{route('mercadolibre.search')}}">
            @csrf
            <div class="field">
                <p class="control has-icons-left has-icons-right">
                    <input class="input" name="search" type="text" placeholder="Buscar...">
                    <span class="icon is-small is-left"><i class="fas fa-search"></i></span>
                </p>
            </div>
        </form>
    </div>

    <div class="column">
        <div class="buttons">
            <button class="button is-primary">Buscar</button>
            <button class="button is-link">Favoritos</button>
        </div>
    </div>
</div>

