<table class="table is-striped">
    <thead>
        <tr>
            <th>Imagen</th>
            <th>Título</th>
            <th>Precio</th>
            <th>Link</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($items as $item)
        <tr>
            <td>{{$item->thumbnail}}</td>
            <td>{{$item->title}}</td>
            <td>{{$item->price}}</td>
            <td>{{$item->permalink}}</td>
            <td>
                
            </td>
        </tr>
        @endforeach
       
    </tbody>
</table>



  'id_ml',
  'title',
  'site_id',
  'price',
  'permalink',
  'thumbnail'