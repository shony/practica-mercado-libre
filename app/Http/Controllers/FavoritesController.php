<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Favorite;

class FavoritesController extends Controller
{
    public function getFavorites()
    {
        $favorites = Favorite::all();
        return response()->json($favorites);
    }

    public function createFavorite(Request $request)
    {
        $favorite = Favorite::create([
            'id_ml'     => $request->id_ml,
            'title'     => $request->title,
            'site_id'   => $request->site_id,
            'price'     => $request->price,
            'permalink' => $request->permalink,
            'thumbnail' => $request->thumbnail
        ]);
        
        return response()->json($favorite);
    }

    public function deleteFavorite(Request $request)
    {
        $favorite = Favorite::find($request->id);
        $favorite->delete();
        return response()->json($favorite);
    }
}
