<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Token;
use MercadoLibreApi;

class MercadoLibreController extends Controller
{
    public function getSearch(Request $request)
    {
        $items = MercadoLibreApi::getSearch($request->search, $request->offset);
        return response()->json($items);
    }

    public function refreshToken()
    {
        $content = MercadoLibreApi::refreshToken();

        // Save New Token
        $token = Token::first();
        if ($token == null) {
            Token::create([
                'access_token'  => $content['access_token'],
                'token_type'    => $content['token_type'],
                'expires_in'    => $content['expires_in'],
                'scope'         => $content['scope'],
                'user_id'       => $content['user_id'],
                'refresh_token' => $content['refresh_token']
            ]);
        } else {
            $token->fill([
                'access_token'  => $content['access_token'],
                'token_type'    => $content['token_type'],
                'expires_in'    => $content['expires_in'],
                'scope'         => $content['scope'],
                'user_id'       => $content['user_id'],
                'refresh_token' => $content['refresh_token']
            ]);
            $token->save();
        }
        

        return response()->json(['success' => true]);
    }
}
