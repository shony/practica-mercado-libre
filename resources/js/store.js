import Vue from 'vue'
import Vuex from 'vuex'

// Modules
import components from './components/store'

Vue.use(Vuex)

const store = new Vuex.Store({
    modules: {
        components
    }
})

export default store
