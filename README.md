## Demo Site
[http://ml.clubplus.com.mx](http://ml.clubplus.com.mx)

## Requirements
```
PHP 7.4
MongoDB Community Server 4.4.5 (https://www.mongodb.com/try/download/community)
MongoDB driver for PHP (https://pecl.php.net/package/mongodb) (v1.9.1) 
(https://www.php.net/manual/en/mongodb.installation.php)
Composer
Node.js v12 or up
```

## Installing Dependencies & Laravel Configuration

```
composer install  
npm install  
cp .env.example .env (Then edit values on .env)
php artisan key:generate 
```

## Edit Mercado Libre Data on .env
MERCADO_LIBRE_AUTH_TOKEN=
MERCADO_LIBRE_APP_ID=
MERCADO_LIBRE_CLIENT_SECRET=
MERCADO_LIBRE_REFRESH_TOKEN=

***
# Compiling

#### Change the url of "MIX_ASSET_URL" on ".env" file. The mix function will prefix the configured URL when generating URLs to assets.
If you work without localhost or urls like that leave it blank.
For localhost example: MIX_ASSET_URL="http://localhost/practica-mercado-libre/public"
For staging & production and others urls leave it in blank, example: MIX_ASSET_URL=

## Development Compiling
```
npm run dev
```

## Watch Development Compiling
```
npm run watch
```

## Production Compiling  
```
npm run prod
```
*** 

## Other Commands
#### Directory Permissions
```
sudo chmod -R 777 storage  
sudo chmod -R 777 bootstrap  
```

