<!doctype html>
<html lang="{{ config('app.locale') }}">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE-edge">
	<meta name="csrf-token" id="csrf-token" content="{{ csrf_token() }}">
	<meta name="public_url" content="{{ url('/') }}/" />
	<meta name="app_name" content="{{ env('APP_NAME') }}" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	{{-- Styles --}}
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">

</head>
<body class="has-navbar-fixed-top">
    {{-- App --}}
    <div id="app" v-cloak>
        <baseapp />
    </div>
	
    {{-- JS --}}
    <script src="{{ mix('js/manifest.js') }}" defer></script>
    <script src="{{ mix('js/vendor.js') }}" defer></script>
    <script src="{{ mix('js/app.js') }}" defer></script>
</body>
</html>
