import Vue from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'

import { faHeart, faHeartBroken, faSearch } from '@fortawesome/free-solid-svg-icons'

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(
    faHeart, faSearch, faHeartBroken
)

Vue.component('font-awesome-icon', FontAwesomeIcon)
