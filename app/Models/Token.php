<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Token extends Model
{
    protected $collection = 'token_collection';
    protected $connection = 'mongodb';

    protected $fillable = [
        'access_token', 'token_type', 'expires_in',
        'scope', 'user_id', 'refresh_token'
    ];
}